import pickle
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


def check_auth(api, version, project_token_file, project_credentials_file, scopes):
    """
    Checks the authentication state for the user.

    :param api: A string that represents the api that we are trying to access.
    :param version: A string that represents the api version that we should use.
    :param project_token_file: Path to the file that stores the app authentification token.
    :param project_credentials_file: Path to the file that stores the credentials file.
    :param scopes: The different scopes for which the app asks permission.

    :return: A google service reference.
    """

    creds = None

    if os.path.exists(project_token_file):
        with open(project_token_file, 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                project_credentials_file, scopes)
            creds = flow.run_local_server()
        with open(project_token_file, 'wb') as token:
            pickle.dump(creds, token)

    service = build(api, version, credentials=creds)

    return service