"""
 Author: @goyoregalado
"""

import random

TEACHER_SUBJECT_CODE = 0
TEACHER_SUBJECT_NAME = 1
TEACHER_FULLNAME = 2
TEACHER_SUBJECT_HOURS = 3
TEACHER_EDUCATIONAL_LEVEL = 4
TEACHER_CLASS_GROUP = 5
TEACHER_EMAIL = 6

STUDENT_ADMINISTRATIVE_CODE = 0
STUDENT_FIRSTNAME = 1
STUDENT_MIDDLENAME = 2
STUDENT_LASTNAME = 3
STUDENT_EDUCATIONAL_LEVEL = 4
STUDENT_CLASS_GROUP = 5
STUDENT_REPEATER = 6
STUDENT_FULL_COURSE = 7
STUDENT_SUBJECT_LIST = 8
STUDENT_PENDING_SUBJECT_LIST = 9

# Safe subset of symbols to be used in password generation as they
# can't be confused with similar glyphs.
LETTERS = u'abcdefghijkmnpqrstwxyzABCDEFGHJKLMNPQRSTWXYZ'
NUMBERS = U'23456789'


def teacher_subject_processing(lines, prefixes_dictionary, sections_dictionary, courseyear):
    """
    Manages the lines of a file that defines the relationship between teachers and subjects and generates an in-memory
    data structure better organized to be used by the rest of our core functions.

    :param lines: Lines of the csv file which contains the definition of the relationship between teachers and
    subjects.
    :param prefixes_dictionary: Normalization dictionary for course prefixes.
    :param sections_dictionary: Normalization dictionary for sections sufixes.
    :param courseyear: A string representation of the course year, for example '19-20'

    :return: A dictionary which keys are the email of every teacher and its used to address a list of subjects.
    """
    output = {}

    for line in lines[1:]:
        email = line[TEACHER_EMAIL].strip()
        subject = line[TEACHER_SUBJECT_CODE].strip()
        subject_name = line[TEACHER_SUBJECT_NAME].strip()
        subject_hours = int(line[TEACHER_SUBJECT_HOURS].strip())
        educational_level = line[TEACHER_EDUCATIONAL_LEVEL].strip()
        denormalized_classgroup = line[TEACHER_CLASS_GROUP].strip()
        teacher_fullname = line[TEACHER_FULLNAME].strip()

        normalized_classgroup_name = normalize_classgroup_name(
                                        denormalized_classgroup,
                                        prefixes_dictionary,
                                        sections_dictionary
                                    )
        
        level = extract_educational_level_code(denormalized_classgroup, prefixes_dictionary)
        section = extract_section_code(denormalized_classgroup, sections_dictionary)

        normalized_subject_code = normalize_subject_code(subject, normalized_classgroup_name, courseyear)

        subject_definition = {
            'SUBJECT_CODE': normalized_subject_code,
            'SUBJECT_NAME': subject_name,
            'SUBJECT_HOURS': subject_hours,
            'CLASS_GROUP': normalized_classgroup_name,
        }
        if email not in output.keys():
            output[email] = {
                'TEACHER_FULLNAME': teacher_fullname,
                'TEACHER_SUBJECT_LIST': [subject_definition]
            }
        else:
            output[email]['TEACHER_SUBJECT_LIST'].append(subject_definition)
    return output


def student_subject_processing(lines, prefixes_dictionary, sections_dictionary, courseyear):
    """
    Manages the lines of a file that defines the relationship between students and subjects an generates a in-memory
    data structure better organized to be used by the rest of our core functions.

    :param lines: Lines of the csv file which contains the definition of the relationship between students and subjects.
    :param prefixes_dictionary: Normalization dictionary for course prefixes.
    :param sections_dictionary: Normalization dictionary for sections sufixes.
    :param courseyear: A string representation of the course year, for example '19-20'
    :return: An structure with a better organization of the same information.
    """
    output = []

    for records in lines[1:]:

        denormalized_classgroup = records[STUDENT_CLASS_GROUP].strip()

        normalized_classgroup_name = normalize_classgroup_name(
            denormalized_classgroup, 
            prefixes_dictionary, 
            sections_dictionary)
                
        level = extract_educational_level_code(denormalized_classgroup, prefixes_dictionary)
        section = extract_section_code(denormalized_classgroup, sections_dictionary)
        try:
            subject_list_records = records[STUDENT_SUBJECT_LIST].split(',')

        except IndexError:
            subject_list_records = []

        #subject_list = [subject.strip() for subject in subject_list_records]
        normalized_subject_list = [normalize_subject_code(
                                                subject.strip(), 
                                                normalized_classgroup_name, 
                                                courseyear) for subject in subject_list_records]
        if normalized_subject_list == ['']:
            normalized_subject_list = []

        try:
            pending_subject_list_records = records[STUDENT_PENDING_SUBJECT_LIST].split(',')
        except IndexError:
            pending_subject_list_records = []

        #pending_subject_list = [subject.strip() for subject in pending_subject_list_records]
        normalized_pending_subject_list = [normalize_pending_subject_code(
                                            subject.strip(),
                                            level,
                                            section,
                                            courseyear) for subject in pending_subject_list_records]
        if normalized_pending_subject_list == ['']:
            normalized_pending_subject_list = []

        student_definition = {
            'STUDENT_ADMINISTRATIVE_CODE': records[STUDENT_ADMINISTRATIVE_CODE].strip(),
            'STUDENT_FIRSTNAME': records[STUDENT_FIRSTNAME].strip(),
            'STUDENT_MIDDLENAME': records[STUDENT_MIDDLENAME].strip(),
            'STUDENT_LASTNAME': records[STUDENT_LASTNAME].strip(),
            'EDUCATIONAL_LEVEL': records[STUDENT_EDUCATIONAL_LEVEL].strip(),
            'CLASS_GROUP': normalized_classgroup_name,
            'STUDENT_REPEATER': records[STUDENT_REPEATER],
            'STUDENT_FULL_COURSE': records[STUDENT_FULL_COURSE],
            'SUBJECT_LIST': normalized_subject_list,
            'PENDING_SUBJECT_LIST': normalized_pending_subject_list,
            'STUDENT_EMAIL': '',
            'STUDENT_PASSWORD': ''
        }
        output.append(student_definition)

    return output


def generate_configuration_dictionary(keys, values):
    """
    Generates a dictionary with configuration values used to translate inputs.
    If the lists passed as arguments haven't the same length raises an ValueError
    exception.

    :param keys: A list of values that will be used as dictionary keys
    :param values: A list of values that will be used as values of the dictionary
    :return: A dictionary using keys an values arguments as key pair elements.
    """
    out = {}

    if len(keys) != len(values):
        raise ValueError

    for i in range(len(keys)):
        out[keys[i]] = values[i]

    return out


def normalize_value(value, translation_dictionary):
    """
    Normalizes the educational level strings present in the input data using the translation dictionary
    :param value: Value that should be normalized.
    :param translation_dictionary: Translation patterns defined as a dictionary.
    :return: A string with the normalized value or an empty string.
    """

    output = ''
    try:
        output = translation_dictionary[value]
    except KeyError:
        output = ''
    return output


def normalize_classgroup_name(classgroup_name, prefixes_dictionary, sections_dictionary):
    """
    Normalizes the classroom name using the translation dictionary keys as a source of
    prefixes.
    :param classgroup_name: The not notmalized classgroup name that we need to translate.
    :param prefixes_dictionary: Translation patterns for the classroom prefixes codes defined as a dictionary.
    :param sections_dictionary: Translation patterns for the classroom section codes defined as a dictionary.

    It would raise an ValueError exception if the value can't be parsed.

    :return: A string with the normalized value or an ValueError exception
    """

    level = extract_educational_level_code(classgroup_name, prefixes_dictionary)
    classgroup_name = clear_values(classgroup_name, prefixes_dictionary)
    section = extract_section_code(classgroup_name, sections_dictionary)
    classgroup_name = clear_values(classgroup_name, sections_dictionary)

    course = extract_course_code(classgroup_name)


    return course + level + section


def normalize_subject_code(subject_code, classgroup_name, courseyear):
    """
    Normalizes the subject code to make possible to match it with the coursenames defined
    in Learning Management Systems.
    :param subject_code: The short code of the subject, usually it's related to an educational administration code.
    :param classgroup_name: The normalized classgroup_name
    :param courseyear: The academic course years range, for example 1819 for 2018-2019 course.
    :return: A string with the normalized subject_code.
    """

    return subject_code + classgroup_name + courseyear


def normalize_pending_subject_code(pending_subject_code, level, section, course_year):
    """
    Normalizes the code of a pending subject.
    In our environment, the pending subject codes are a string defined by the administrative code of the subject
    concatenated with a number that represents the course where the student didn't pass the subject.
    For example: SSB1
    :param pending_subject_code: The pending subject code as received from the administration software.
    :param level: Educational level of the subject.
    :param section: Section of the class group where the student is assigned.
    :param course_year: The academic course years range, for example 1819 for 2018-2019 course.
    :return: A string with the normalized subject_code corresponding to an existent course.
    """
    normalized_pending_subject = ''
    try:
        course = pending_subject_code[-1]

        classgroup_name = course + level + section
        subject_code = pending_subject_code[:-1]

        normalized_pending_subject = normalize_subject_code(subject_code, classgroup_name, course_year)

        return normalized_pending_subject
    except IndexError:
        return normalized_pending_subject


def extract_educational_level_code(classgroup_name, classgroup_prefixes_dictionary):
    """
    Extracts educational level code from a denormalized classgroup_name string
    :param classgroup_name: Denormalized classgroup name string
    :param classgroup_prefixes_dictionary: Translation patterns for the classroom prefixes codes defined as a dictionary.
    :return: A string with the educational code or raises an ValueError exception
    """

    prefixes = classgroup_prefixes_dictionary.keys()

    for prefix in prefixes:
        if prefix in classgroup_name:
            level = classgroup_prefixes_dictionary[prefix]
            return level
    raise ValueError


def extract_section_code(classgroup_name, section_prefixes_dictionary):
    """
    Extracts the section code from a denormalized classgroup_name string
    :param classgroup_name: Denormalized classgroup name string
    :param section_prefixes_dictionary: Translation patterns for the sections prefixes codes defined as a dictionary.
    :return: A string with the educational code or raises an ValueError exception
    """

    sections = section_prefixes_dictionary.keys()

    for section in sections:
        if section in classgroup_name:
            return section_prefixes_dictionary[section]
    raise ValueError


def extract_course_code(classgroup_name):
    """
    Extracts the number that represents the course from a denormalized classgroup_name string
    :param classgroup_name: Denormalized classgroup name string
    :return: Returns a string with the course code or a ValueError if this course code is not a number
    """
    course = classgroup_name[:1]

    try:
        int(course)
        return course
    except ValueError:
        raise ValueError


def clear_values(classgroup_name, translation_dictionary):
    """
    Removes the values of the translation_dictionary from a denormalized classgroup_name string
    if they are present.
    :param classgroup_name: Denormalized classgroup name string
    :param translation_dictionary: Translation patterns that we want to remove from the denormalized
    classgroup_name.
    :return: The original string with a denormalized classgroup_name without the substring
            related to the translation_dictionary or raises an ValueError exception
    """
    values = translation_dictionary.keys()

    for value in values:
        if value in classgroup_name:
            cleared_classgroup_name = classgroup_name.replace(value, '')
            return cleared_classgroup_name
    raise ValueError



def compare_records(previous_record, actual_record):
    """
        Compares two records to check if the fields that could determine without ambiguity
        if they represent the same person even though the other fields differ.

        :param previous_record: The old record that we want to check.
        :param actual_record: The new record

        :return: A boolean that will be True if the records represents the same person.
        and will be False if the records presumably represents different persons.
    """
    if previous_record['STUDENT_ADMINISTRATIVE_CODE'] == actual_record['STUDENT_ADMINISTRATIVE_CODE']:
        return True
    return False

def extract_administrative_code_list(students_records):
    """
        Extract all the administrative codes that are present into a list of student records
        it returns a set with the whole list.

        :param students_records: A list of student records

        :return: A python set with all the different administrative codes present in the list.
    """
    out = set()
    for record in students_records:
        if record['STUDENT_ADMINISTRATIVE_CODE']:
            out.add(record['STUDENT_ADMINISTRATIVE_CODE'])
    return out

def random_password_generation(minlength=8, maxlength=8):
    """
        Generates a random password of minimum length of minlength and maximum length of maxlength 
        characters.

        :param minlength: Desired minimum length for the passwords that we are generating.
        :param maxlength: Desired maximum length for the passwords that we are generating.

        :return: A string representing a new password generated using a safe subset of symbols and
        between the lengths specified.
    """

    # First of all we normalize the arguments when the
    # situation is ilogic.

    if minlength > maxlength:
        maxlength = minlength
    if maxlength < minlength:
        minlength = maxlength
    out = ""
    max_letters = len(LETTERS)
    max_numbers = len(NUMBERS)
    # We always generate a password which length is between
    # minLength and maxLength.
    passLength = random.randint(minlength, maxlength)
    for i in range(passLength):
        if(random.randint(0,1)):
            out += LETTERS[random.randrange(max_letters)]
        else:
            out += NUMBERS[random.randrange(max_numbers)]
    return out