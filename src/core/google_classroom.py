import pickle
import copy
import os.path
import simplejson


from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = [
            'https://www.googleapis.com/auth/classroom.courses',
            'https://www.googleapis.com/auth/classroom.profile.emails',
            'https://www.googleapis.com/auth/classroom.rosters',
            'https://www.googleapis.com/auth/classroom.profile.photos'
        ]


def parse_course_record(service, record):
    """
    Gets a record from classroom and outputs it in a more convenient format.
    It also queries the course alias and append it to the correspondent field.

    :param service: Google service reference
    :param record: The google classroom record.
    :return: Returns a dictionary with just the needed fields
    """
    out = {}
    out['ID'] = record['id']
    out['OWNERID'] = record['ownerId']
    out['NAME'] = record['name']
    try:
        out['LONGNAME'] = record['descriptionHeading']
    except KeyError:
        out['LONGNAME'] = None

    try:
        out['SECTION'] = record['section']
    except KeyError:
        out['SECTION'] = None

    out['CREATION_TIME'] = record['creationTime']
    out['UPDATE_TIME'] = record['updateTime']
    out['STATUS'] = record['courseState']
    out['GUARDIANSENABLED'] = record['guardiansEnabled']
    out['ENROLLMENTCODE'] = record['enrollmentCode']

    aliases = get_course_aliases(service, out['ID'])
    if not aliases:
        out['ALIASES'] = []
    else:
        for alias in aliases:
            out = add_course_alias_record(out, alias)

    return out


def add_course_alias_record(record, alias):
    out = copy.deepcopy(record)
    try:
        out['ALIASES'].append(alias)
    except KeyError:
        out['ALIASES'] = [alias]
    return out


def parse_alias_record(record):
    """
    Gets an alias record directly from classroom and returns an equivalent string.
    Alias records have this structure:
        {'alias': 'd:SRC2INA1819'}
    We should strip the 'd:' string

    :param record: The record that we want to parse

    :return: A string with the alias or an empty string.
    """
    out = ''
    try:
        out = record['alias'][2:]
    except KeyError:
        out = ''
    return out


def get_courses(service, max_results=500):
    """
    Gets the list of courses defined in the domain.

    :param service: Google service reference
    :param max_results: The maximum number of records that we want to retrieve
    :return: A list of courses or an empty list in a more convenient format.
    """
    out = []
    last_record = False
    first_record = True
    request = None
    response = None

    while not last_record:
        if first_record:
            request = service.courses().list(pageSize=max_results)
            first_record = False
        else:
            request = service.courses().list_next(previous_request=request, previous_response=response)
        if request:
            response = request.execute()
            courses = response.get('courses', [])
            for course in courses:
                out.append(parse_course_record(service, course))
        else:
            last_record = True
    return out


def get_course_by_id(service, course_id, debug=False):
    """
    Get the whole data of a course identified by course_id

    :param service: Google service reference
    :param course_id: Identifier string for the course we are interested in.
    :return: The course detail as returned by the parse_course_record function
    """
    out = {}
    try:
        request = service.courses().get(id=course_id)
        response = request.execute()
        out = parse_course_record(service, response)
    except HttpError:
        if (debug):
            print('Course %s not found' % (course_id))
    return out


def get_course_by_alias(service, course_alias, debug=False):
    """
    Get the whole data of a course identified by course_alias

    :param service: Google service reference
    :param course_alias: Alias of the course, it's a string composed by 
    SUBJECT_CODE + CLASSGROUP + COURSE_YEARS
    :return: The course detail as returned by the parse_course_record function
    """
    out = {}
    try:
        request = service.courses().get(id='d:' + course_alias)
        response = request.execute()
        out = parse_course_record(service, response)
    except HttpError:
        if (debug):
            print('Course %s not found' % (course_alias))
    return out


def get_course_aliases(service, course_id, max_results=500):
    """
    Returns the list of aliases asigned to the course identified by
    course_id.

    :param service: Google service reference
    :param course_id: Identifier string for the course we are interested in.
    :param max_results: The maximum number of records that we want to retrieve


    :return: A list of aliases or an empty list.
    """
    out = []
    last_record = False
    first_record = True

    request = None
    response = None

    while not last_record:
        if first_record:
            request = service.courses().aliases().list(courseId=course_id, pageSize=max_results)
            first_record = False
        else:
            request = service.courses().aliases().list_next(previous_request=request, previous_response=response)
        if request:
            response = request.execute()
            for alias_record in response.get('aliases', []):
                out.append(parse_alias_record(alias_record))
        else:
            last_record = True

    return out


def get_student_courses(service, user_id):
    """
    Gets the list of courses where the user_id is enrolled as student.

    :param service: Google service reference
    :param user_id: Email of the student we are asking for.
    :return: A list of courses or an empty list in a more convenient format.
    """
    out = []
    results = service.courses().list(studentId=user_id).execute()
    courses = results.get('courses', [])
    for course in courses:
        out.append(parse_course_record(service, course))
    return out


def get_teacher_courses(service, user_id):
    """
    Gets the list of courses where the user_id is enrolled as teacher.

    :param service: Google service reference
    :param user_id: Email of the teacher we are asking for.
    :return: A list of courses or an empty list in a more convenient format.
    """

    out = []
    results = service.courses().list(teacherId=user_id).execute()
    courses = results.get('courses', [])
    for course in courses:
        out.append(parse_course_record(service, course))
    return out


def create_course_alias(service, course_id, course_alias, debug=False):
    """
        Adds an alias to the course identified by course_id.

        :param service: Google service reference.
        :param course_id: Google classroom course identifier.
        :param course_alias: The alias that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error

        :return: Returns a boolean. True if the alias has been created. Otherwise returns
        False.
    """
    output = False
    request_body = {
        'alias': 'd:' + course_alias
    }

    try:
        service.courses().aliases().create(courseId=course_id, body=request_body).execute()
        output = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to create alias %s for course %s' % (course_alias, course_id))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return output


def create_course(service, course_name, class_group, subject_code, owner_email, debug=False):
    """
        Creates a new course assigning the user referenced by owner_email as its
        owner. Also adds the subject_code as an alias for this course.

        :param service: Google service reference
        :param course_name: The name that we want to stablish for this course.
        :param class_group: The class group code of the class where this course is been teached.
        :param subject_code: The subject_code as represented at the output of core.normalize_subject_code
        function.
        :param owner_email: The email which represents the user that will be the owner of this course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: Returns a dictionary with a course record as returned by the  
    """
    out = {}
    request_body = {
        'name': course_name,
        'section': class_group,
        'ownerId': owner_email
    }
    try:
        request = service.courses().create(body=request_body)
        response = request.execute()
        out = parse_course_record(service, response)
        create_course_alias(service, out['ID'], subject_code, debug)
        out = get_course_by_alias(service, subject_code)
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to create course %s' % (course_name))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))

    return out


def delete_course_by_id(service, course_id, debug=False):
    """
        Deletes a course from google classroom.

        :param service: Google service reference.
        :param course_id: Google classroom course identifier.
        :param debug: A boolean that determines if we are going to show verbose error

        :return: Returns a boolean, True if the course has been deleted, False otherwise.
    """
    out = False
    try:
        request = service.courses().delete(id=course_id)
        response = request.execute()
        out = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to delete by id course %s' % (course_id))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def delete_course_by_alias(service, course_alias, debug=False):
    """
        Deletes a course which has course_alias assigned as an alias.

        :param service: Google service reference.
        :param course_alias: An alphanumeric alias assigned to the course 
        that we want to delete.
        :param debug: A boolean that determines if we are going to show verbose error
    """
    out = False
    try: 
        request = service.courses().delete(id='d:' + course_alias).execute()
        out = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to delete by alias course %s' % (course_alias))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def add_teacher_to_course(service, course_id, teacher_email, debug=False):
    """
        Generic method to add a teacher to a preexistent course, its used by
        add_teacher_to_course_by_id and add_teacher_to_course_by_alias functions
        and these are the preferred methods to add teachers.

        :param service: Google service reference.
        :param course_id: Identifier of the course where we want to add a teacher.
        :param teacher_email: Email of the teacher that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error

        :return: A dictionary which stores the raw response obtained from classroom api or
        an empty dictionary if there have been any errors.
    """
    out = {}
    request_body = {
        'userId': teacher_email
    }
    try:
        request = service.courses().teachers().create(courseId=course_id, body=request_body)
        out = request.execute()
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            if error.get('code') == 409:
                print('Teacher %s was assigned to course %s before' % (teacher_email, course_id))
            else:
                print('Error trying to add teacher %s to course %s' % (teacher_email, course_id))
                print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))    
    return out


def add_teacher_to_course_by_id(service, course_id, teacher_email, debug=False):
    """
        Adds a teacher to a course identified by its id, not its alias.

        :param service: Google service reference.
        :param course_id: The id of the course where we want to add a teacher.
        :param teacher_email: The email of the teacher that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been added to the course, False otherwise.
    """
    out = False
    response = add_teacher_to_course(service, course_id, teacher_email, debug)
    if response:
        full_name = response.get('profile').get('name').get('fullName')    
        print('Added teacher %s to course %s by id' % (full_name, course_id))
        out = True
    else:
        if debug:
            print('Error trying to add teacher %s to course %s by id' % (teacher_email, course_id))
    return out


def add_teacher_to_course_by_alias(service, course_alias, teacher_email, debug=False):
    """
        Adds a teacher to a course identified by its alias

        :param service: Google service reference.
        :param course_alias: The alias of the course where we want to add a teacher.
        :param teacher_email: The email of the teacher that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been added to the course, False otherwise.
    """
    out = False
    course_id = 'd:'+ course_alias
    response = add_teacher_to_course(service, course_id, teacher_email, debug)
    if response:
        full_name = response.get('profile').get('name').get('fullName')    
        print('Added teacher %s to course %s by alias' % (full_name, course_alias))
        out = True
    else:
        if debug:
            print('Error trying to add teacher %s to course %s by id' % (teacher_email, course_id))
    return out


def remove_teacher_from_course(service, course_id, teacher_email, debug=False):
    """
        This is a generic function to remove a teacher from a course, it is used
        by remove_teacher_from_course_by_id and remove_teacher_from_course_by_alias,
        these are the preferred methods to remove a teacher from a course.

        :param service: Google service reference.
        :param course_id: Identifier of the course where we want to remove a teacher.
        :param teacher_email: Email of the teacher that we want to remove from the course.
        :param debug: A boolean that determines if we are going to show verbose error

        :return: True if the teacher has been removed, False otherwise.
    """
    out = False
    try:
        request = service.courses().teachers().delete(courseId=course_id, userId=teacher_email)
        request.execute()
        out = True
        print('Teacher %s removed from course %s' % (teacher_email, course_id))
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error trying to remove teacher %s from course %s' % (teacher_email, course_id))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def remove_teacher_from_course_by_id(service, course_id, teacher_email, debug=False):
    """
        Removes a teacher to a course identified by its id, not its alias.

        :param service: Google service reference.
        :param course_id: The id of the course where we want to remove a teacher.
        :param teacher_email: The email of the teacher that we want to remove from the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been removed from the course, False otherwise.
    """
    out = False

    response = remove_teacher_from_course(service, course_id, teacher_email, debug)
    if response:
        out = True
    else:
        if debug:
            print('Error trying to remove teacher %s to course %s by id' % (teacher_email, course_id))
    
    return out


def remove_teacher_from_course_by_alias(service, course_alias, teacher_email, debug=False):
    """
        Removes a teacher to a course identified by its alias.

        :param service: Google service reference.
        :param course_alias: The alias of the course where we want to remove a teacher.
        :param teacher_email: The email of the teacher that we want to remove from the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been removed from the course, False otherwise.
    """
    out = False
    course_id = 'd:' + course_alias

    response = remove_teacher_from_course(service, course_id, teacher_email, debug)
    if response:
        out = True
    else:
        if debug:
            print('Error trying to remove teacher %s to course %s by alias' % (teacher_email, course_id)) 
    return out


def change_course_owner_by_id(service, course_id, course_name, teacher_email, debug=False):
    """
        Changes the course owner and set it to the one identified by teacher_email argument.

        :param service: Google service reference.
        :param course_id: The id of the course where we want to remove a teacher.
        :param course_name: The name of the course, its a mandatory parameter.
        :param teacher_email: The email of the teacher that we want to remove from the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been removed from the course, False otherwise.
    """
    out = False

    request_body = {
        'name': course_name,
        'ownerId': teacher_email,
    }

    try:
        request = service.courses().update(id=course_id, body=request_body)
        response = request.execute()
        if response:
            out = True
        if debug:
            print('Course %s reasigned to teacher %s' % (course_id, teacher_email))

    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error trying to change owner of course %s to teacher %s' % (course_id, teacher_email))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def change_course_owner_by_alias(service, course_alias, course_name, teacher_email, debug=False):
    """
        Changes the course owner and set it to the one identified by teacher_email argument.

        :param service: Google service reference.
        :param course_alias: The alias of the course where we want to remove a teacher.
        :param course_name: The name of the course, its a mandatory parameter.
        :param teacher_email: The email of the teacher that we want to remove from the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been removed from the course, False otherwise.
    """
    out = False

    request_body = {
        'name': course_name,
        'ownerId': teacher_email,
    }

    try:
        request = service.courses().update(id='d:' + course_alias, body=request_body)
        response = request.execute()
        if response:
            out = True
        if debug:
            print('Course %s reasigned to teacher %s' % (course_alias, teacher_email))

    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error trying to change owner of course %s to teacher %s' % (course_alias, teacher_email))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def add_student_to_course(service, course_id, student_email, debug=False):
    """
        Generic method to add a student to a preexistent course, its used by
        add_student_to_course_by_id and add_student_to_course_by_alias functions
        and these are the preferred methods to add student.

        :param service: Google service reference.
        :param course_id: Identifier of the course where we want to add a teacher.
        :param student_email: Email of the student that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error

        :return: A dictionary which stores the raw response obtained from classroom api or
        an empty dictionary if there have been any errors.
    """
    out = {}

    request_body = {
        'userId': student_email
    }

    try:
        request = service.courses().students().create(
                            courseId=course_id,
                            body=request_body, 
                        )
        out = request.execute()
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            if error.get('code') == 409:
                print('Student %s its already enrolled at course %s' % (student_email, course_id))
            else:
                print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))

    return out

def add_student_to_course_by_id(service, course_id, student_email, debug=False):
    """
        Adds a student to a course identified by its id, not its alias.

        :param service: Google service reference.
        :param course_id: The id of the course where we want to add a teacher.
        :param student_email: The email of the student that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the student has been added to the course, False otherwise.
    """
    out = False
    response = add_student_to_course(service, course_id, student_email, debug)
    if response:
        full_name = response.get('profile').get('name').get('fullName')    
        print('Added student %s to course %s by id' % (full_name, course_id))
        out = True
    else:
        if debug:
            print('Error trying to add student %s to course %s by id' % (student_email, course_id))
    return out

def add_student_to_course_by_alias(service, course_alias, student_email, debug=False):
    """
        Adds a student to a course identified by its alias

        :param service: Google service reference.
        :param course_alias: The alias of the course where we want to add a teacher.
        :param student_email: The email of the student that we want to add to the course.
        :param debug: A boolean that determines if we are going to show verbose error.

        :return: True if the teacher has been added to the course, False otherwise.
    """
    out = False
    course_id = 'd:'+ course_alias
    response = add_student_to_course(service, course_id, student_email, debug)
    if response:
        full_name = response.get('profile').get('name').get('fullName')    
        print('Added student %s to course %s by alias' % (full_name, course_alias))
        out = True
    else:
        if debug:
            print('Error trying to add student %s to course %s by id' % (student_email, course_id))
    return out

def main():
    """
    Shows basic usage of the Classroom API.
    Prints the names of the first 10 courses the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('../../credentials/classroom-token.pickle'):
        with open('../../credentials/classroom-token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '../../credentials/classroom-credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('../../credentials/classroom-token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('classroom', 'v1', credentials=creds)

    #courses = get_courses(service)
    #print('Total courses: %d' % (len(courses)))

    #create_course(service, 'Rantanplánidos', '1RANTAN', '1RANTAN1819', 'test@wonderfullschool.net', debug=True)

    #add_teacher_to_course_by_id(service, '37159249832', 'testteacher@wonderfullschool.net', True)
    
    #remove_teacher_from_course_by_alias(service, '1RANTAN1819', 'testteacher@wonderfulschool.net', True)
    #delete_course_by_id(service, '37159239202', debug=True)
    
    #delete_course_by_alias(service, '1RANTAN1819')

    #change_course_owner_by_alias(service, '1RANTAN1819', 'Super rantan course','othertestteacher@wonderfullschool.net', debug=True)
    change_course_owner_by_id(service, '37161494953', 'Digital cuissine', 'testteacher@wonderfulschool.net', debug=True)

    teacher_courses = get_teacher_courses(service, 'testteacher@wonderfulschool.net')
 
    for course in teacher_courses:
        print('\n\n')
        print(course)
        print("Los aliases")
        print(get_course_aliases(service, course['ID']))


    # print('\n\n')
    # get_course_by_id(service, 'rantanplan', True)
    # print(get_course_by_id(service, '3999407', True))

    # print('By Alias')

    # print(get_course_by_alias(service, '1RDE1819'))

    #for course in teacher_courses:
    #    print(get_course_aliases(service, course_id=course['ID']))


if __name__ == '__main__':
    main()
