import os
import hashlib
import copy
from core.core import compare_records, extract_administrative_code_list

def has_changed(previous_hash, data):
    """
    Checks if the data has changed since last check
    :param previous_hash: The hash computed in the last test
    :param data: The new data as obtained from the input and without processing.
    :return: A boolean. True if the data has changed, false otherwise.
    """
    new_hash = hashlib.sha512(str(data).encode('utf-8'))
    hex_digest = new_hash.hexdigest()

    if hex_digest == previous_hash:
        return False
    else:
        return True

def record_has_changed(previous_record, record):
    """
    Check if a record has changed since the last check
    :param previous_record: The record from the last sincronization
    :param record: The new record that we want to check
    :return: A boolean. True if the data has changed, false otherwise.
    """
    previous_record_string = str(previous_record).encode('utf-8')
    record_string = str(record).encode('utf-8')

    previous_hash = hashlib.sha512(previous_record_string).hexdigest()
    new_hash = hashlib.sha512(record_string).hexdigest()

    if previous_hash == new_hash:
        return False
    else:
        return True


def get_modified_records(previous_records, actual_records):
    """
    It goes across actual_records and computes the hash of every row.
    Then it looks into previous_records hash table.
    Every record which its hash isn't a key into the previous_record is
    an modified record or a brand new record.
    On the other hand every record that it's not present into the actual_records
    its a record marked for deletion.

    :param previous_records: A hash dictionary that stores every single row of
    the input data indexed by hash.
    :param actual_records: A matrix with raw data as obtained from input.
    :return: A dictionary with three keys: 
        - modified: Which stores a list of modified records.
        - not_present: Which stores a list of records marked for deletion.
    """
    previous_records_copy = copy.deepcopy(previous_records)

    valid_hashes = previous_records.keys()
    out = {
        'new': [],
        'modified': [],
        'not_present': []
    }

    for actual_record in actual_records:
        actual_hash = hashlib.sha512(str(actual_record).encode('utf-8')).hexdigest()
        if actual_hash not in valid_hashes:
            # At this point it can be a brand new record or a modified record
            # which hash value has changed consecuently.
            match = False
            previous_record_match_hash = None
            for previous_record_hash in previous_records_copy:
                if compare_records(previous_records_copy[previous_record_hash], actual_record):
                    out['modified'].append(actual_record)
                    previous_record_match_hash = previous_record_hash
                    match = True
                    break
            if not match:
                out['new'].append(actual_record)
            else:
                del(previous_records_copy[previous_record_match_hash])
        else:
            del(previous_records_copy[actual_hash])
            

    #administrative_code_list = extract_administrative_code_list(out['modified'])
    
    # Now we should clean up the previous_records which corresponds to modified records
    for record in previous_records_copy:
        # We check if the records that are still present in the previous_records_copy
        # which are the ones that hasn't a match are one of the modified.
        # To do so we check if the key values are the same using a function of the core
        # library, maybe in the future this process will be more fine grained.
        out['not_present'].append(previous_records_copy[record])

    return out

def process_modified_records(modified_records):
    """
        This is an very first aproximation to the problem it will return the 
        same register without modification to generate the new hash table.
        But in the future it should check if the account was generated and then
        create the email, enrol into the courses, etc ...
    """
    out = modified_records
    return out


def generate_hash_table(records):
    """
    Generates a hash table for the records present into the list passed as an argument.
    :param records: A list of dictionaries that we want to index into a hash table.
    :return: A dictionary indexed by record hashes.
    """
    out = {}

    for record in records:
        record_hash = hashlib.sha512(str(record).encode('utf-8')).hexdigest()
        out[record_hash] = record

    return out




