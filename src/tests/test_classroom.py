import os
import unittest
import json

from unittest.mock import MagicMock
from googleapiclient.discovery import build

from googleapiclient.http import HttpMock, HttpRequestMock, RequestMockBuilder


from core.google_classroom import parse_course_record, parse_alias_record, add_course_alias_record

DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')

def datafile(filename):
    return os.path.join(DATA_DIR, filename)


class TestGoogleClassroomInterface(unittest.TestCase):

    def setUp(self):
        self.http = HttpMock(filename=datafile('classroom.json'), headers={"status": "200"})

    def test_parse_course_record(self):
        """
        Parses a record from google classroom and outputs it in a more convenient way.
        """
        
        aliases_response = {'aliases': [{'alias': 'd:supermaths'}]}

        aliases_response_json = json.dumps(aliases_response)

        requestBuilder = RequestMockBuilder({

            'classroom.courses.aliases.list': (None, aliases_response_json),
            'classroom.courses.aliases.list_next': (None, None)
        })

        service = build('classroom', 'v1', http=self.http, requestBuilder=requestBuilder)

        record = {
            'id': '023894092384',
            'name': 'MATHS',
            'section': 'HIGHSCHOOL',
            'descriptionHeading': 'MATHS ARE COOL',
            'room': 'Classroom 3',
            'ownerId': '0849302489230948',
            'creationTime': '2019-05-09T08:53:28.296Z',
            'updateTime': '2019-05-09T09:08:42.199Z',
            'enrollmentCode': '2f7onyi',
            'courseState': 'ACTIVE',
            'alternateLink': 'https://classroom.google.com/c/KSJDHFKJHS2',
            'teacherGroupEmail': 'maths_teachers@wonderfullschool.net',
            'courseGroupEmail': 'maths_324234234@wonderfullschool.net',
            'teacherFolder': {
                'id': 'jskljadhflksadhjflahflkjfhlksdjhflksfhlskdfjh'
            },
            'guardiansEnabled': False,
            'calendarId': 'wonderfullschool.net_classroomskfjhkshdf@group.calendar.google.com'
        }

        output = parse_course_record(service, record)

        desired_output = {}
        
        desired_output['ID'] = record['id']
        desired_output['OWNERID'] = record['ownerId']
        desired_output['NAME'] = record['name']
        desired_output['LONGNAME'] = record['descriptionHeading']
        desired_output['SECTION'] = record['section']
        desired_output['CREATION_TIME'] = record['creationTime']
        desired_output['UPDATE_TIME'] = record['updateTime']
        desired_output['STATUS'] = record['courseState']
        desired_output['GUARDIANSENABLED'] = record['guardiansEnabled']
        desired_output['ENROLLMENTCODE'] = record['enrollmentCode']
        desired_output['ALIASES'] = ['supermaths']

        self.assertEqual(desired_output, output)
    
    def test_parse_alias_record(self):

        alias_record = {'alias': 'd:TEE1ESB1718'}
        output = parse_alias_record(alias_record)
        desired_output = 'TEE1ESB1718'
        self.assertEqual(desired_output, output)
    
    def test_parse_alias_record_empty(self):

        alias_record = {}
        output = parse_alias_record(alias_record)
        desired_output = ''
        self.assertEqual(desired_output, output)

    def test_add_course_alias(self):
        record = {
            'id': '023894092384',
            'name': 'MATHS',
            'section': 'HIGHSCHOOL',
            'descriptionHeading': 'MATHS ARE COOL',
            'room': 'Classroom 3',
            'ownerId': '0849302489230948',
            'creationTime': '2019-05-09T08:53:28.296Z',
            'updateTime': '2019-05-09T09:08:42.199Z',
            'enrollmentCode': '2f7onyi',
            'courseState': 'ACTIVE',
            'alternateLink': 'https://classroom.google.com/c/KSJDHFKJHS2',
            'teacherGroupEmail': 'maths_teachers@wonderfullschool.net',
            'courseGroupEmail': 'maths_324234234@wonderfullschool.net',
            'teacherFolder': {
                'id': 'jskljadhflksadhjflahflkjfhlksdjhflksfhlskdfjh'
            },
            'guardiansEnabled': False,
            'calendarId': 'wonderfullschool.net_classroomskfjhkshdf@group.calendar.google.com'
        }

        #output = parse_course_record(record)
        output = add_course_alias_record(record, 'PHYSICS')
        desired_aliases = ['PHYSICS']

        self.assertEqual(desired_aliases, output['ALIASES'])


