import unittest

from core.core import teacher_subject_processing, student_subject_processing, generate_configuration_dictionary, \
                    normalize_value, normalize_classgroup_name, normalize_subject_code, \
                    normalize_pending_subject_code, extract_educational_level_code, extract_section_code, \
                    extract_course_code, compare_records, random_password_generation, \
                    clear_values


class TestCoreFunctionality(unittest.TestCase):

    def test_groups_teacher_subjects(self):
        """
        If there's a couple of lines representing different subjects for the same teacher, they should be grouped.
        """
        lines = [['SUBJECT','SUBJECTNAME','TEACHER','HOURS','LEVEL','CLASS','EMAIL']]
        lines.append(['BIG', 'BIOLOGY', 'DOE, JOHN', '3', 'K12', '1ºK12-A', 'johndoe@wonderfullschool.net'])
        lines.append(['PHY', 'PHYSICS', 'DOE, JOHN', '3', 'K12', '1ºK12-A', 'johndoe@wonderfullschool.net'])

        prefixes_dictionary = {
            'K12': 'K12',
            'K13': 'K13',
            'ESOUNI': 'ESO'
        }

        sections_dictionary = {
            'A': 'A',
            'B': 'B'
        }

        courseyear = '1920'

        output = teacher_subject_processing(lines, prefixes_dictionary, sections_dictionary, courseyear)

        desired_output = {
            'johndoe@wonderfullschool.net': {
                'TEACHER_FULLNAME': 'DOE, JOHN',
                'TEACHER_SUBJECT_LIST': [
                    {
                        'SUBJECT_CODE': 'BIG1K12A1920',
                        'SUBJECT_NAME': 'BIOLOGY',
                        'SUBJECT_HOURS': 3,
                        'CLASS_GROUP': '1K12A',
                    },
                    {
                        'SUBJECT_CODE': 'PHY1K12A1920',
                        'SUBJECT_NAME': 'PHYSICS',
                        'SUBJECT_HOURS': 3,
                        'CLASS_GROUP': '1K12A',
                    }
                ]
            }
        }

        self.assertEqual(desired_output, output)

    def test_process_student_lines(self):
        """
        It generates a normalized list of students related with the subjects they are registered in.
        """
        lines = [['ADMINCODE', 'FIRSTNAME', 'MIDDLENAME', 'LASTNAME', 'LEVEL', 'CLASS', 'REPEATER',
                'FULLCOURSE', 'SUBJECTLIST', 'PENDINGSUBJECTLIST']]
        lines.append(['11111', 'JOHN', 'DOE', 'DOE', 'K12', '1K12-A', 'Sí', 'No', 'BYG, PHY, MAT', 'LNT2, PHY1'])
        lines.append(['22222', 'JANE', 'DOE', 'DOE', 'K13', '1K13-B', 'Yes', 'No', 'BYG, ETH, PHI', ''])
        
        prefixes_dictionary = {
            'K12': 'K12',
            'K13': 'K13',
            'ESOUNI': 'ESO'
        }

        sections_dictionary = {
            'A': 'A',
            'B': 'B'
        }


        output = student_subject_processing(lines, prefixes_dictionary, sections_dictionary, '1920')

        desired_output = [
            {
                'STUDENT_ADMINISTRATIVE_CODE': '11111',
                'STUDENT_FIRSTNAME': 'JOHN',
                'STUDENT_MIDDLENAME': 'DOE',
                'STUDENT_LASTNAME': 'DOE',
                'EDUCATIONAL_LEVEL': 'K12',
                'CLASS_GROUP': '1K12A',
                'STUDENT_REPEATER': 'Sí',
                'STUDENT_FULL_COURSE': 'No',
                'SUBJECT_LIST': ['BYG1K12A1920', 'PHY1K12A1920', 'MAT1K12A1920'],
                'PENDING_SUBJECT_LIST': ['LNT2K12A1920', 'PHY1K12A1920'],
                'STUDENT_EMAIL': '',
                'STUDENT_PASSWORD': ''
            },
            {
                'STUDENT_ADMINISTRATIVE_CODE': '22222',
                'STUDENT_FIRSTNAME': 'JANE',
                'STUDENT_MIDDLENAME': 'DOE',
                'STUDENT_LASTNAME': 'DOE',
                'EDUCATIONAL_LEVEL': 'K13',
                'CLASS_GROUP': '1K13B',
                'STUDENT_REPEATER': 'Yes',
                'STUDENT_FULL_COURSE': 'No',
                'SUBJECT_LIST': ['BYG1K13B1920', 'ETH1K13B1920', 'PHI1K13B1920'],
                'PENDING_SUBJECT_LIST': [],
                'STUDENT_EMAIL': '',
                'STUDENT_PASSWORD': ''
            }
        ]
        self.maxDiff = None

        self.assertEqual(desired_output, output)
    
    def test_compare_records_same_person(self):
        previous_record = {
                'STUDENT_ADMINISTRATIVE_CODE': '11111',
                'STUDENT_FIRSTNAME': 'JOHN',
                'STUDENT_MIDDLENAME': 'DOE',
                'STUDENT_LASTNAME': 'DOE',
                'EDUCATIONAL_LEVEL': 'K12',
                'CLASS_GROUP': '1ºK12-A',
                'STUDENT_REPEATER': 'Sí',
                'STUDENT_FULL_COURSE': 'No',
                'SUBJECT_LIST': ['BYG', 'PHY', 'MAT'],
                'PENDING_SUBJECT_LIST': ['LNT2', 'PHY1'], 
            }

        actual_record = {
                'STUDENT_ADMINISTRATIVE_CODE': '11111',
                'STUDENT_FIRSTNAME': 'JOSEPH',
                'STUDENT_MIDDLENAME': 'DOE',
                'STUDENT_LASTNAME': 'DOE',
                'EDUCATIONAL_LEVEL': 'K12',
                'CLASS_GROUP': '1ºK12-A',
                'STUDENT_REPEATER': 'Sí',
                'STUDENT_FULL_COURSE': 'No',
                'SUBJECT_LIST': ['BYG', 'PHY', 'MAT'],
                'PENDING_SUBJECT_LIST': ['LNT2', 'PHY1']
        }
    
        output = compare_records(previous_record, actual_record)

        desired_output = True

        self.assertEqual(desired_output, output)
    
    def test_compare_records_different_person(self):
        previous_record = {
            'STUDENT_ADMINISTRATIVE_CODE': '11111',
            'STUDENT_FIRSTNAME': 'JOHN',
            'STUDENT_MIDDLENAME': 'DOE',
            'STUDENT_LASTNAME': 'DOE',
            'EDUCATIONAL_LEVEL': 'K12',
            'CLASS_GROUP': '1ºK12-A',
            'STUDENT_REPEATER': 'Sí',
            'STUDENT_FULL_COURSE': 'No',
            'SUBJECT_LIST': ['BYG', 'PHY', 'MAT'],
            'PENDING_SUBJECT_LIST': ['LNT2', 'PHY1']
        }

        actual_record = {
                'STUDENT_ADMINISTRATIVE_CODE': '22222',
                'STUDENT_FIRSTNAME': 'JOSEPH',
                'STUDENT_MIDDLENAME': 'DOE',
                'STUDENT_LASTNAME': 'DOE',
                'EDUCATIONAL_LEVEL': 'K12',
                'CLASS_GROUP': '1ºK12-A',
                'STUDENT_REPEATER': 'Sí',
                'STUDENT_FULL_COURSE': 'No',
                'SUBJECT_LIST': ['BYG', 'PHY', 'MAT'],
                'PENDING_SUBJECT_LIST': ['LNT2', 'PHY1']
        }

        output = compare_records(previous_record, actual_record)

        desired_output = False

        self.assertEqual(desired_output, output)
    
    def test_extract_administrative_code_list(self):
        self.fail('Implemented but not tested!!!')

class TestNormalization(unittest.TestCase):

    def test_generate_configuration_dictionary_same_length_lists(self):

        keys = ['Kindergarten', 'Under 12 years', 'High School']
        values = ['KDS', 'K-12', 'HIGH']
        desired_output = {
            'Kindergarten': 'KDS',
            'Under 12 years': 'K-12',
            'High School': 'HIGH'
        }

        output = generate_configuration_dictionary(keys, values)

        self.assertEqual(desired_output, output)

    def test_generate_configuration_dictionary_not_same_length_lists(self):

        keys = ['Kindergarten', 'Under 12 years', 'High School']
        values = ['KDS', 'K-12']

        self.assertRaises(ValueError, generate_configuration_dictionary, values, keys)


    def test_normalize_educational_level(self):

        translation_dictionary = {
            'Kindergarten': 'KDS',
            'Under 12 years': 'K-12',
            'High School': 'HIGH'
        }
        desired_output = 'KDS'

        output = normalize_value('Kindergarten', translation_dictionary)

        self.assertEqual(desired_output, output)

    def test_normalize_educational_level_value_not_present(self):
        translation_dictionary = {
            'Kindergarten': 'KDS',
            'Under 12 years': 'K-12',
            'High School': 'HIGH'
        }
        desired_output = ''

        output = normalize_value('Sophomore', translation_dictionary)

        self.assertEqual(desired_output, output)

    def test_normalize_class_group(self):

        prefixes_dictionary = {
            'Freshman': 'FRESH',
            'Shophomore': 'SHOPH',
            'UnderGraduate': 'UNDER'
        }

        sections_dictionary = {
            'PA': 'A',
            'PB': 'B'
        }

        output = normalize_classgroup_name('2ShophomorePB', prefixes_dictionary, sections_dictionary)
        desired_output = '2SHOPHB'
        self.assertEqual(desired_output, output)

    def test_normalize_class_group_wrong_format(self):

        prefixes_dictionary = {
            'Freshman': 'FRESH',
            'Shophomore': 'SHOPH',
            'UnderGraduate': 'UNDER'
        }

        sections_dictionary = {
            'PA': 'A',
            'PB': 'B'
        }

        self.assertRaises(ValueError, normalize_classgroup_name, 'ABPRIPB', prefixes_dictionary, sections_dictionary)

    def test_normalize_subjects(self):

        course_code = 'PHYS'
        classgroup_name = '1UNDERA'
        course_year = '1819'

        output = normalize_subject_code(course_code, classgroup_name, course_year)

        desired_output = 'PHYS1UNDERA1819'

        self.assertEqual(desired_output, output)

    def test_normalize_pending_subjects(self):
        pending_subject_code = 'BIG1'
        level = 'UNDER'
        section = 'C'
        course_year = '1819'

        output = normalize_pending_subject_code(pending_subject_code, level, section, course_year)

        desired_output = 'BIG1UNDERC1819'

        self.assertEqual(desired_output, output)

    def test_extract_educational_level_code_exists(self):

        prefixes_dictionary = {
            'Freshman': 'FRESH',
            'Shophomore': 'SHOPH',
            'UnderGraduate': 'UNDER'
        }

        denormalized_classgroup_code = '2ShophomorePB'

        output = extract_educational_level_code(denormalized_classgroup_code, prefixes_dictionary)

        desired_output = 'SHOPH'

        self.assertEqual(desired_output, output)

    def test_extract_educational_level_code_doesnt_exists(self):

        prefixes_dictionary = {
            'Freshman': 'FRESH',
            'Shophomore': 'SHOPH',
            'UnderGraduate': 'UNDER'
        }

        denormalized_classgroup_code = '2GraduatedPB'

        self.assertRaises(ValueError, extract_educational_level_code, denormalized_classgroup_code, prefixes_dictionary)

    def test_extract_section_code_exists(self):

        sections_dictionary = {
            'PA': 'A',
            'PB': 'B'
        }

        denormalized_classgroup_name = '2GraduatedPB'

        output = extract_section_code(denormalized_classgroup_name, sections_dictionary)

        desired_output = 'B'

        self.assertEqual(desired_output, output)

    def test_extract_section_code_doesnt_exist(self):

        sections_dictionary = {
            'PA': 'A',
            'PB': 'B'
        }

        denormalized_classgroup_name = '2GraduatedPC'

        self.assertRaises(ValueError, extract_section_code, denormalized_classgroup_name, sections_dictionary)

    def test_extract_course_code(self):

        denormalized_classgroup_name = '2GraduatedPC'

        output = extract_course_code(denormalized_classgroup_name)

        desired_output = '2'

        self.assertEqual(desired_output, output)

    def test_extract_course_code_wrong_format(self):

        denormalized_classgroup_name = 'GraduatedPC'

        self.assertRaises(ValueError, extract_course_code, denormalized_classgroup_name)
    
    def test_clear_values(self):
        denormalized_classgroup_name = 'INF24PB'
        prefixes_dictionary = {
            'INF2': 'INF',
            'PRI1': 'PRI',
            'ESOUNI': 'ESO'
        }
        cleared_classgroup = clear_values(denormalized_classgroup_name, prefixes_dictionary)
        desired_output = '4PB'
        self.assertEqual(cleared_classgroup, desired_output)



class TestPasswordUtilities(unittest.TestCase):

    def test_random_password_generation_not_default_lengths(self):

        min_length = 3
        max_length = 6
        output = random_password_generation(min_length, max_length)

        self.assertEqual(len(output) > min_length, True)
        self.assertEqual(len(output) < max_length, True)
