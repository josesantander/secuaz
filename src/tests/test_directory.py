import unittest

from core.google_directory import parse_user_record, check_user_exist_full_name, \
                                    check_user_exists_admin_id, get_user_accounts, max_id, \
                                    parse_group_record, get_group_from_group_list


class TestGoogleDirectoryInterface(unittest.TestCase):

    def test_parse_user_records_without_external_id(self):
        """
        Parses a record from google directory and outputs it in a more convenient way.
        """
        record = {'kind': 'admin#directory#user', 'id': '1234556677888',
         'etag': '"thisisanETAG"',
         'primaryEmail': 'john.doe@wonderfulschool.net',
         'name': {'givenName': 'JOHN', 'familyName': 'DOE', 'fullName': 'JOHN DOE'},
         'isAdmin': False, 'isDelegatedAdmin': False, 'lastLoginTime': '2017-06-22T10:11:10.000Z',
         'creationTime': '2017-03-14T19:05:43.000Z', 'agreedToTerms': True, 'suspended': True,
         'suspensionReason': 'ADMIN', 'archived': False, 'changePasswordAtNextLogin': False, 'ipWhitelisted': False,
         'emails': [{'address': 'john.doe@wonderfulschool.net', 'primary': True}],
         'nonEditableAliases': ['john.doe@wonderfulschool.net.test-google-a.com'], 'customerId': 'C=====0',
         'orgUnitPath': '/', 'isMailboxSetup': True, 'isEnrolledIn2Sv': False, 'isEnforcedIn2Sv': False,
         'includeInGlobalAddressList': True}

        output = parse_user_record(record)

        desired_output = {
            'EMAIL': 'john.doe@wonderfulschool.net',
            'ID': '1234556677888',
            'FULLNAME': 'JOHN DOE',
            'GIVENNAME': 'JOHN',
            'FAMILYNAME': 'DOE',
            'ADMIN': False,
            'SUSPENDED': True,
            'ARCHIVED': False,
            'EDUCATIVE_ADMINISTRATION_CODE': None
        }

        self.assertEqual(desired_output, output)

    def test_user_exist_user_not_exist(self):
        """
        Test if a user exist when the person hasn't an account in the system.
        """

        user_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'jane.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JANE WICK',
                'GIVENNAME': 'JANE',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'lovedogs@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            }
        ]

        output = check_user_exist_full_name('BRUCE WAYNE', user_list)
        desired_output = []

        self.assertEqual(desired_output, output)

    def test_user_exist_user_exists(self):
        """
        Test if a user exist when the person has an account in the system.
        """

        user_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'jane.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JANE WICK',
                'GIVENNAME': 'JANE',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'lovedogs@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            }
        ]

        output = check_user_exist_full_name('JOHN DOE', user_list)
        desired_output = ['john.doe@wonderfulschool.net',]

        self.assertEqual(desired_output, output)

    def test_user_exist_user_multiple_accounts(self):
        """
        Test if a user exist when the person has more than one account in the system.
        """

        user_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'jane.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JANE WICK',
                'GIVENNAME': 'JANE',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            },
            {
                'EMAIL': 'lovedogs@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False
            }
        ]

        output = check_user_exist_full_name('JOHN WICK', user_list)
        desired_output = ['john.wick@wonderfulschool.net', 'lovedogs@wonderfulschool.net']

        self.assertEqual(desired_output, output)

    def test_user_exist_check_full_name_when_user_exists(self):
        """
        Checks if a user exist in a user list
        """

        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }

        ]

        output = check_user_exist_full_name('JOHN WICK', fake_users_list)

        desired_output = ['john.wick@wonderfulschool.net', ]

        self.assertEqual(desired_output, output)

    def test_user_exist_check_full_name_when_user_not_exists(self):
        """
        Checks if a user exist in a user list when this user haven't an account
        """

        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }

        ]

        output = check_user_exist_full_name('BRUCE WAYNE', fake_users_list)

        desired_output = []

        self.assertEqual(desired_output, output)

    def test_user_exist_check_full_name_when_user_multiple_accounts(self):
        """
        Checks if a user exist in a user list when she exists and has multiple accounts
        """

        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            },
            {
                'EMAIL': 'lovedogs.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': None
            }
        ]

        output = check_user_exist_full_name('JOHN WICK', fake_users_list)

        desired_output = ['john.wick@wonderfulschool.net', 'lovedogs.wick@wonderfulschool.net']

        self.assertEqual(desired_output, output)

    def test_check_user_exist_by_externalID_when_user_exists(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            },
            {
                'EMAIL': 'lovedogs.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': None
            }
        ]

        output = check_user_exists_admin_id('11111', fake_users_list)

        desired_output = ['john.doe@wonderfulschool.net']

        self.assertEqual(desired_output, output)

    def test_check_user_exist_by_externalID_when_user_not_exists(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            },
            {
                'EMAIL': 'lovedogs.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': None
            }
        ]

        output = check_user_exists_admin_id('77777', fake_users_list)

        desired_output = []

        self.assertEqual(desired_output, output)

    def test_check_user_exist_by_externalID_when_user_multiple_accounts(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            },
            {
                'EMAIL': 'lovedogs.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': None
            },
            {
                'EMAIL': 'alternative.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            }
        ]

        output = check_user_exists_admin_id('11111', fake_users_list)

        desired_output = ['john.doe@wonderfulschool.net', 'alternative.doe@wonderfulschool.net']

        self.assertEqual(desired_output, output)

    def test_get_user_accounts_user_defined_by_admin_id(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }
        ]

        output = get_user_accounts('', '33333', fake_users_list)

        desired_output = ['tony.stark@wonderfulschool.net']

        self.assertEqual(desired_output, output)

    def test_get_user_accounts_user_defined_by_full_name(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }
        ]

        output = get_user_accounts('TONY STARK', '', fake_users_list)

        desired_output = ['tony.stark@wonderfulschool.net']

        self.assertEqual(desired_output, output)


    def test_get_user_accounts_doesnt_duplicate_emails(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }
        ]

        output = get_user_accounts('TONY STARK', '33333', fake_users_list)

        desired_output = ['tony.stark@wonderfulschool.net']

        self.assertEqual(desired_output, output)

    def test_get_user_accounts_user_doesnt_exist(self):
        fake_users_list = [
            {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'tony.stark@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }
        ]

        output = get_user_accounts('BRUCE WAYNE', '999999', fake_users_list)

        desired_output = []

        self.assertEqual(desired_output, output)
    
    def test_max_id(self):
        fake_users_list = [
            {
                'EMAIL': 'usr2018@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
            },
            {
                'EMAIL': 'john.wick@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN WICK',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'WICK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '22222'
            },
            {
                'EMAIL': 'usr9998@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'TONY STARK',
                'GIVENNAME': 'TONY',
                'FAMILYNAME': 'STARK',
                'ADMIN': False,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '33333'
            }
        ]
        default_prefix = 'usr' 
        max_prefix_length = 4
        
        output = max_id(fake_users_list, default_prefix, max_prefix_length)

        desired_output = 9998


        self.assertEqual(desired_output, output)
    
    def test_parse_group_record(self):

        group_record = {
            'id': 'abcdef',
            'email': 'fakegroup@wonderfulschool.net',
            'name': 'Super group',
            'description': 'This is a group for testing purposes',
            'directMembersCount': '100'
        }

        desired_output = {
            'ID': 'abcdef',
            'EMAIL': 'fakegroup@wonderfulschool.net',
            'NAME': 'Super group',
            'DESCRIPTION': 'This is a group for testing purposes',
            'NUM_MEMBERS': 100
        }

        output = parse_group_record(group_record)

        self.assertEqual(desired_output, output)
    
    def test_parse_group_record_without_users(self):
        group_record = {
            'id': 'abcdef',
            'email': 'fakegroup@wonderfulschool.net',
            'name': 'Super group',
            'description': 'This is a group for testing purposes',
        }

        desired_output = {
            'ID': 'abcdef',
            'EMAIL': 'fakegroup@wonderfulschool.net',
            'NAME': 'Super group',
            'DESCRIPTION': 'This is a group for testing purposes',
            'NUM_MEMBERS': 0
        }

        output = parse_group_record(group_record)

        self.assertEqual(desired_output, output)
    
    def test_get_group_from_group_list(self):
        group_list = [
            {
            'ID': 'abcdef',
            'EMAIL': 'fakegroup@wonderfulschool.net',
            'NAME': 'Super group',
            'DESCRIPTION': 'This is a group for testing purposes',
            'NUM_MEMBERS': 0
            },
            {
            'ID': 'abcdef',
            'EMAIL': 'hipergroup@wonderfulschool.net',
            'NAME': 'Hiper group',
            'DESCRIPTION': 'This is another group for testing purposes',
            'NUM_MEMBERS': 1000
            }
        ]

        group = 'Super group'

        output = get_group_from_group_list(group_list, group)

        desired_output = {
            'ID': 'abcdef',
            'EMAIL': 'fakegroup@wonderfulschool.net',
            'NAME': 'Super group',
            'DESCRIPTION': 'This is a group for testing purposes',
            'NUM_MEMBERS': 0
        }
        
        self.assertEqual(desired_output, output)

    def test_get_group_from_group_list_doesnt_exist(self):
        group_list = [
            {
            'ID': 'abcdef',
            'EMAIL': 'fakegroup@wonderfulschool.net',
            'NAME': 'Super group',
            'DESCRIPTION': 'This is a group for testing purposes',
            'NUM_MEMBERS': 0
            },
            {
            'ID': 'abcdef',
            'EMAIL': 'hipergroup@wonderfulschool.net',
            'NAME': 'Hiper group',
            'DESCRIPTION': 'This is another group for testing purposes',
            'NUM_MEMBERS': 1000
            }
        ]

        group = 'Mega group'

        output = get_group_from_group_list(group_list, group)

        desired_output = {}
        
        self.assertEqual(desired_output, output)


