import unittest

import pickle
import os.path
from googleapiclient.discovery import build

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from core.google_spreadsheets import get_spreadsheet_data

TEST_SPREADSHEET_ID = '1bavR0FxjtOCc6pFc4IqXym4Ol0hTWa-zXeshYNctWmU'
TEST_SPREADSHEET_RANGE = 'Hoja 1!A1:L4000'

class TestGoogleSpreadsheetsInterface(unittest.TestCase):

    def test_drive_data_download(self):
        """
        Access a spreadsheet downloaded from google Drive and returns the values in a list of list format.
        """
        SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('../credentials/spreadsheet-token.pickle'):
            with open('../credentials/spreadsheet-token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    '../credentials/spreadsheet-credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('../credentials/spreadsheet-token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)

        values = get_spreadsheet_data(service, TEST_SPREADSHEET_ID, TEST_SPREADSHEET_RANGE)

        desired_values = [
            ['Edu Admin Code', 'First name', 'Middle name', 'Last Name', 'Level', 'Class', 'Repeater', 'Full course', 'Subject list',
             'Pending Subject List'],
            ['11111', 'Ted', 'Doe', 'Doe', 'K12', '1ºK12-A', '', '', 'BIG, HST, ETH'],
            ['22222', 'Lorna', 'Doe', 'Doe', 'K13', '2ºK13-B', '', '', 'PHY, PHI, MAT', 'PHY1, MAT1']

        ]
        self.assertEqual(desired_values, values)