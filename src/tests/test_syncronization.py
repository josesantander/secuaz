import unittest
import hashlib

from core.syncronization import has_changed, record_has_changed, get_modified_records, generate_hash_table

class TestSyncronization(unittest.TestCase):
    def test_has_changed(self):
        data = [
            {'name': 'John', 'age': 32},
            {'name': 'Jane', 'age': 21}
        ]

        previous_hash = hashlib.sha512(str(data).encode('utf-8')).hexdigest()

        output = has_changed(previous_hash, data)

        desired_output = False

        self.assertEqual(desired_output, output)

    def test_has_changed_with_changes(self):
        data = [
            {'STUDENT_ADMINISTRATIVE_CODE': '111111', 'STUDENT_FIRSTNAME': 'John', 'STUDENT_LASTNAME': 'Doe'},
            {'STUDENT_ADMINISTRATIVE_CODE': '111111', 'STUDENT_FIRSTNAME': 'Jane', 'STUDENT_LASTNAME': 'Doe'}
        ]

        previous_hash = 'a fake hash'

        output = has_changed(previous_hash, data)

        desired_output = True

        self.assertEqual(desired_output, output)

    def record_has_changed_with_changes(self):

        previous_record = {
            'EMAIL': 'john.doe@wonderfulschool.net',
            'ID': '1234556677888',
            'FULLNAME': 'JOHN DOE',
            'GIVENNAME': 'JOHN',
            'FAMILYNAME': 'DOE',
            'ADMIN': False,
            'SUSPENDED': True,
            'ARCHIVED': False,
            'EDUCATIVE_ADMINISTRATION_CODE': '11111'
        }

        record = {
                'EMAIL': 'john.doe@wonderfulschool.net',
                'ID': '1234556677888',
                'FULLNAME': 'JOHN DOE',
                'GIVENNAME': 'JOHN',
                'FAMILYNAME': 'DOE',
                'ADMIN': True,
                'SUSPENDED': True,
                'ARCHIVED': False,
                'EDUCATIVE_ADMINISTRATION_CODE': '11111'
        }

        output = record_has_changed(previous_record, record)

        desired_output = True

        self.assertEqual(desired_output, output)

    def record_has_changed_without_changes(self):
        previous_record = {
            'EMAIL': 'john.doe@wonderfulschool.net',
            'ID': '1234556677888',
            'FULLNAME': 'JOHN DOE',
            'GIVENNAME': 'JOHN',
            'FAMILYNAME': 'DOE',
            'ADMIN': False,
            'SUSPENDED': True,
            'ARCHIVED': False,
            'EDUCATIVE_ADMINISTRATION_CODE': '11111'
        }

        record = {
            'EMAIL': 'john.doe@wonderfulschool.net',
            'ID': '1234556677888',
            'FULLNAME': 'JOHN DOE',
            'GIVENNAME': 'JOHN',
            'FAMILYNAME': 'DOE',
            'ADMIN': False,
            'SUSPENDED': True,
            'ARCHIVED': False,
            'EDUCATIVE_ADMINISTRATION_CODE': '11111'
        }

        output = record_has_changed(previous_record, record)

        desired_output = False

        self.assertEqual(desired_output, output)

    def test_get_modified_records(self):
        
        record1 = {'STUDENT_ADMINISTRATIVE_CODE': '111111', 'STUDENT_FIRSTNAME': 'John', 'STUDENT_LASTNAME': 'Doe'}
        record1_hash = hashlib.sha512(str(record1).encode('utf-8')).hexdigest()

        record2 = {'STUDENT_ADMINISTRATIVE_CODE': '111111', 'STUDENT_FIRSTNAME': 'Jane', 'STUDENT_LASTNAME': 'Doe'}
        record2_hash = hashlib.sha512(str(record2).encode('utf-8')).hexdigest()
        
        previous_data = {
            record1_hash: record1,
            record2_hash: record2
        }

        record1['STUDENT_FIRSTNAME'] = 'Joseph'

        actual_data = [
            record1,
            record2
        ]

        output = get_modified_records(previous_data, actual_data)

        desired_output = {'modified': [record1,], 'not_present': []}

        self.assertEqual(desired_output, output)

    def test_get_modified_records_not_modified(self):
        record1 = {'STUDENT_ADMINISTRATIVE_CODE': '111111', 'STUDENT_FIRSTNAME': 'John', 'STUDENT_LASTNAME': 'Doe'}
        record1_hash = hashlib.sha512(str(record1).encode('utf-8')).hexdigest()

        record2 = {'STUDENT_ADMINISTRATIVE_CODE': '111111', 'STUDENT_FIRSTNAME': 'Jane', 'STUDENT_LASTNAME': 'Doe'}
        record2_hash = hashlib.sha512(str(record2).encode('utf-8')).hexdigest()

        previous_data = {
            record1_hash: record1,
            record2_hash: record2
        }

        actual_data = [
            record1,
            record2
        ]

        output = get_modified_records(previous_data, actual_data)

        desired_output = {'modified': [], 'not_present': []}

        self.assertEqual(desired_output, output)

    def test_generate_hash_table(self):
        record1 = {'FirstName': 'John', 'LastName': 'Doe'}
        record1_hash = hashlib.sha512(str(record1).encode('utf-8')).hexdigest()
        record2 = {'FirstName': 'Jane', 'LastName': 'Doe'}
        record2_hash = hashlib.sha512(str(record2).encode('utf-8')).hexdigest()

        records = [record1, record2]

        output = generate_hash_table(records)

        desired_output = {
            record1_hash: record1,
            record2_hash: record2
        }

        self.assertEqual(desired_output, output)
    
    def test_get_modified_records(self):
        self.fail("Get modified records didn't work properly when we eliminate or add new students")